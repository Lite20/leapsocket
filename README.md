# Leap Socket#
A cross-platform library for network communication, leveraging different technologies such as TCP, Websockets, and Bluetooth.
Primary concerns are execution speed and simplicity, but memory usage and security are also high-priority.

Leap Socket currently supports Windows, Linux, Mac, and Android implementations of:
- tcp
- udp
- websockets
	
We're looking to extend support to more languages such as Java and Python, and are looking to leverage technologies such as Bluetooth. Contributions are greatly appreciated.