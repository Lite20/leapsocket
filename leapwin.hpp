#pragma once

#include<stdio.h>
#include<winsock2.h>

#pragma comment(lib, "ws2_32.lib") // Winsock Library

#define BUFLEN 512
struct leapwin {
    SOCKET s;

    struct sockaddr_in server;
    struct sockaddr_in si_other;

    int slen;
    int recv_len;

    char buf[BUFLEN];

    WSADATA wsa;

    void (*handler)();
    void (*errr)(int);

    void fetch()
    {
        fflush(stdout);

        // clear the buffer by filling null, it might have previously received data
        memset(buf, '\0', BUFLEN);

        // try to receive some data, this is a blocking call
        if ((recv_len = recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen)) == SOCKET_ERROR)
        {
            printf("recvfrom() failed with error code : %d" , WSAGetLastError());
            errr(1);
        }

        // call handler
        handler();
    }

    void send(const char* buffer, sockaddr_in& target)
    {
        if (sendto(s, buffer, strlen(buffer), 0, (struct sockaddr *) &target, sizeof(target)) == SOCKET_ERROR)
        {
            printf("sendto() failed with error code : %d" , WSAGetLastError());
            errr(2);
        }
    }

    void close()
    {
        closesocket(s);
        WSACleanup();
    }

    leapwin(int port, void (*handle)(), void (*err)(int))
    {
        slen = sizeof(si_other);

        handler = handle;
        errr = err;

        // Initialise winsock
        if (WSAStartup(MAKEWORD(2,2),&wsa) != 0)
        {
            printf("Failed. Error Code : %d",WSAGetLastError());
            errr(3);
        }

        // Create a socket
        if((s = socket(AF_INET, SOCK_DGRAM, 0)) == INVALID_SOCKET)
            printf("Could not create socket : %d" , WSAGetLastError());

        // Prepare the sockaddr_in structure
        server.sin_family = AF_INET;
        server.sin_addr.s_addr = INADDR_ANY;
        server.sin_port = htons(port);

        // Bind
        if(bind(s, (struct sockaddr *) &server, sizeof(server)) == SOCKET_ERROR)
        {
            printf("Bind failed with error code : %d", WSAGetLastError());
            errr(4);
        }
    }

    ~leapwin()
    {
        close();
    }
};
